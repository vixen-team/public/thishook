from datetime import datetime

EVENT = "Merge Request Hook"


def run(request, color, authorHidden, branchHidden):
    content = request.get_json()

    repo = content["project"]["name"]
    repo_url = content["object_attributes"]["url"]
    isPrivate = content["project"]["visibility_level"]
    source_branch = content["changes"]["source_branch"]["current"]
    target_branch = content["changes"]["target_branch"]["current"]
    mr_author = content["user"]["username"]
    mr_author_avatar = content["user"]["avatar_url"]
    mr_title = content["object_attributes"]["title"]
    mr_description = content["object_attributes"]["description"]
    iid = content["object_attributes"]["iid"]

    if any(
        x in mr_description
        for x in ("githook:ignore", "gh:i", "thishook:ignore", "th:i")
    ):
        return

    data = {
        "embeds": [
            {
                "timestamp": datetime.now().isoformat(),
                "author": {
                    "name": f"New merge request opened - !{iid}"
                    if authorHidden
                    else f"{mr_author} opened a new merge request - !{iid}",
                },
                "color": color,
            }
        ]
    }

    if isPrivate != 0:
        data["embeds"][0]["author"]["url"] = repo_url

    if not branchHidden:
        # got tired and couldn't think of a way to properly handle these few duplicate lines so
        if "description" not in data["embeds"][0]:
            data["embeds"][0][
                "description"
            ] = f"`{repo}:{source_branch}` 🡒 `{repo}:{target_branch}`"
        else:
            data["embeds"][0][
                "description"
            ] += f"`{repo}:{source_branch}` 🡒 `{repo}:{target_branch}`"

    if not authorHidden:
        data["embeds"][0]["author"]["icon_url"] = mr_author_avatar

    if any(
        x in mr_description
        for x in ("githook:private", "gh:p", "thishook:private", "th:p")
    ):
        if "description" not in data["embeds"][0]:
            data["embeds"][0][
                "description"
            ] = "\n\nThis merge request was marked as private."
        else:
            data["embeds"][0][
                "description"
            ] += "\n\nThis merge request was marked as private."
    else:
        for private_string in ["githook:private", "gh:p", "thishook:private", "th:p"]:
            mr_description = mr_description.replace(private_string, "")

        data["embeds"][0]["fields"] = [
            {
                "name": mr_title if len(mr_title) <= 256 else mr_title[:250] + "...",
                "value": mr_description
                if len(mr_description) <= 1024
                else mr_description[:1020] + "...",
            }
        ]

    return data
